% Execute a maximum linear-phase SPSP pulse design for 1.5 or 3T:
% we want to suppress fat, with some shim area around it
% we want to excite water with a linear phase vs f around it
% we want the slope of the phase in the excited band to be as large
% as possible, within reasonable error and peak RF
% 
% outside requirements to run this script:
% 1) Fessler's toolbox
% 2) John Pauly's RF toolbox
%
% Copyright 2009-4-27, Will Grissom, Stanford University
%

% switch between maximum linear-phase and conventional linear-phase 
domaxphase = 1;

gambar = 4258;       % gamma/2pi in Hz/g
gam = gambar*2*pi;   % gamma in radians/g

field = 1.5; % field strength for design

% default parameters for 1.5T
if field == 1.5
  nlobes = 8; % 8 for 1.5T spsp 
  Ts = 0.0015; %0.0015; % sec, spectral sampling period
  B0 = 15000;
elseif field == 3 % default params for 3T
  nlobes = 7;
  Ts = 0.001016;
  B0 = 30000;
end

tipangle = 30; % tip angle of iterative spsp pulse
roughpenalty = 1; % switch to use roughness penalty, to avoid 
                  % 'conolly wings'
roughbeta = 10; % roughness regularization param

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spectral parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
shim = 0.4e-6;	  % Conservative shim requirement
fat1 = 0.9e-6;
fat2 = 1.3e-6;
water = 4.7e-6;			
alpha = -0.01e-6; % ppm/deg Celsius
max_temp = 70;    % max temp
base_temp = 36.8; % Celsius

% Convert to frequency
fspec = B0 * ([(fat1-shim) (fat2+shim) (water+(max_temp-base_temp)*alpha-shim) (water+shim)]-water) * gambar;

slthickz = 1; % cm, z slice thickness

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get gradient waveform
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
z_tb = 4; % time-bandwidth of z-rf will tell us how long our
          % gradient should be
kz_w = z_tb / slthickz; % cycles/cm
gz_area = kz_w / 4257; % G/cm * s
dt = 4e-6; % gradient/rf sampling rate
nsamp = ceil(Ts/dt); % 
verse_frac = 0.9;
gmax = 4; % G/cm, max grad amp
gslew = 15; % G/cm/ms max slew
flyback = 1;
[gpos, gneg, g1, g2, g3] = grad_ss(gz_area, nsamp, verse_frac, gmax, ...
                                   gslew, dt, ~flyback);
g = [];
% stitch together the gradient. Build an RF mask as well.
rfmask = [];
for ii = 1:nlobes
  if ii < nlobes
    g = [g gpos gneg];
    rfmask = [rfmask zeros(size(g1)) ones(size(g2)) zeros(size(g3)) ...
             zeros(size(gneg))];
  else
    g = [g gpos]; % don't want to add refocusing lobe at end, we'll
                  % just build an optimal one for z.
    rfmask = [rfmask zeros(size(g1)) ones(size(g2)) ...
              zeros(size(g3))];
  end
end
% design final refocusing lobe to place kz=0 at the center of the
% positive traps
garea = gz_area/2; % G/cm*s
gref = grad_mintrap(garea, gmax, gslew, dt);
g = [g -gref];
rfmask = [rfmask zeros(size(gref))];
rfmask = logical(rfmask);

% integrate gradient to get kz(t)
gupsampfact = 4;
gint = interp(g,gupsampfact);
kz = -gambar * dt/gupsampfact * fliplr(cumsum(fliplr(gint),2));
kz = kz(1:gupsampfact:end);
kzd = kz(rfmask);

% get kf
tt = (0:dt:length(g)*dt-dt);
kf = tt-(length(g)-1)*dt;
kfd = kf(rfmask);

% select desired effective TE period at end of pulse
% max phase pulse will have TE starting 3/4 of the way through the
% first grad lobe
TEeff_m = length(g)*dt*1000 - (length(gpos)+length(gneg))*3/4*dt*1000; % ms
% linear phase pulse will have TE starting 1/2 way through the
% pulse
TEeff_l = length(g)*dt*1000 - (nlobes-1)/2*Ts*1000 - length(gpos)/2*dt*1000; % ms

if ~domaxphase; TEeff = TEeff_l;else;TEeff = TEeff_m;end;
disp(['Effective echo time (TEeff): ' num2str(TEeff) ' ms']);
disp(['TE difference between lin-phase and max-phase: ' ...
     num2str(TEeff_m-TEeff_l) ' ms']);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build desired pattern and error weights
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
osfact = 8; % design grid oversampling factor
fovz = 1/dt / gambar / mean(g2);
Nz = length(g2)*osfact;

d1 = 0.01;d2 = 0.01; % pass- and stop-band ripples (in normalized
                     % flip angle units)
di = dinf(d1,d2); % Dinfinity function - gives transition width in z
w = di/z_tb; % transition width in z
f = [0 (1-w)*(z_tb/2) (1+w)*(z_tb/2) (length(g2)/2)]/(length(g2)/2); % normalized
                                                     % frequency edges
w = [1 d1/d2]; % pass- and stopband weights

zedges = f * fovz / 2; % edges of pass- and stopband

% set up the design grid
fovf = 2*max(abs(fspec));
Nf = ceil(fovf*length(g)*dt)*osfact; % number of spectral grid points
if rem(Nf,2) ~= 0
  Nf = Nf + 1;
end
[zz,ff] = ndgrid(-fovz/2:fovz/Nz:fovz/2-fovz/Nz,[-fovf/2:fovf/Nf:fovf/2-fovf/Nf]);

% z dim of desired pattern
dzp = double(abs(zz) <= zedges(2)); % pass band mask
dzs = double(abs(zz) >= zedges(3)); % stop band mask
wz = w(1) * dzp + w(2) * dzs;

% build error weighting matrix
maskw = double(fspec(3) <= ff & ff <= fspec(4)).*(wz.^2) + ...
        double(fspec(1) <= ff & ff <= fspec(2));
W = diag_sp(col(maskw));

% desired pattern in spectral dim
df = ((fspec(3) <= ff) & (ff <= fspec(4)));
df = df.*exp(-1i*TEeff*2*pi/1000*ff);

% combine dims
d = df.*dzp;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build system matrix and roughness penalty and perform design
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
J = 6;Kz = 2*Nz;Kf = 2*Nf; % nufft params
nufft_args = {[Nz Nf],[J J],[Kz Kf],[Nz Nf]/2,'minmax:kb'}; % nufft params
G = Gmri_SENSE([kzd.' kfd.'],logical(ones([Nz Nf])),'fov',[fovz fovf], ...
               'basis',{'dirac'},'nufft',nufft_args,'exact',0)';

if roughpenalty
  % get roughness penalty
  R = (roughbeta)*spdiags(single([ones(sum(rfmask),1) -ones(sum(rfmask),1)]),[0 1],sum(rfmask),sum(rfmask)); 
else
  R = 0;
end

iters = ceil(sum(rfmask)/5); % # of CG iterations
disp(['Number of CG iterations: ' num2str(iters)]);

% run CG to design pulse
[xS,info] = qpwls_pcg(zeros(length(kzd),1),G,W,d,0, ...
                      R,1,iters,ones(size(d)));

rf = zeros(size(rfmask));
rf(rfmask) = xS(:,end)*tipangle*pi/180/(1i*gam*dt);

% get final mag pattern
m = G * xS(:,end);m = reshape(m,Nz,Nf);
disp(['Design NRMSE: ' num2str(nrmse(d(:),m(:),maskw(:)))]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulate pulse and display results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

simus = 2; % simulation upsamp factor

% simulate at z = 0 across spectral dim
fsim = -fovf/2:fovf/Nf/simus:fovf/2-fovf/Nf/simus;
[foo,mbl_f,mblz_f] = blochsim_spinor_slsel(Nf*simus,fovf,dt,rf,2*pi/gam*ones(length(rf),1));

% simulate at f = 0 across z dim
zsim = -fovz/2:fovz/Nz/simus:fovz/2-fovz/Nz/simus;
[foo,mbl_z,mblz_z] = blochsim_spinor_slsel(Nz*simus,fovz,dt,rf,g);

figure;
% note that the magnitude should be high in water band, and zero in fat
% band, but doesn't matter elsewhere
plot(ff(1,:),sin(abs(d(Nz/2+1,:))*tipangle*pi/180),'k');
hold on, grid on
plot(fsim,abs(mbl_f),'r');
title 'Spectral magnitude profile, z = 0'
xlabel 'f (Hz)'
ylabel '|M_{xy}|'
legend('Desired','Simulated');

figure;
% note that the phase should match in the water band, but doesn't
% matter elsewhere
plot(ff(1,:),angle(d(Nz/2+1,:)),'k');
hold on, grid on
plot(fsim,angle(mbl_f),'r');
title 'Spectral phase profile, z = 0'
xlabel 'f (Hz)'
ylabel '\angle M_{xy}'
legend('Desired','Simulated');

figure
% note that in this plot the simulated profile will look much
% broader than the desired, since the transition regions are not depicted
title 'Slice profile, f = 0'
plot(zz(:,Nf/2+1),sin(abs(d(:,Nf/2+1))*tipangle*pi/180),'k');
grid on,hold on
plot(zsim,abs(mbl_z),'r');
legend('Desired','Simulated');

figure;grid on
plot(1000*tt,real(rf),'b');
hold on
plot(1000*tt,imag(rf),'r');
xlabel 'Time (ms)'
ylabel 'b_1 (Gauss)'







