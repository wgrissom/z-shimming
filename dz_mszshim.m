% Spectral spatial pulse design script
function [rf,g,d,m,TEeff,greflen] = dz_mszshim(params)

if nargin == 0
    params.gmax = 3;
    params.gslew = 12;
    params.passmean = 150;
    params.passw = 100;
    params.stopmax = 200;
    params.tbf = 4;
    params.tbz = 4;
    params.slthickz = 0.4;%cm
    params.dt = 6.4e-6;
    params.display = 1;
    params.tipangle = 60;
    params.flyback = 1;
end

flyback = params.flyback;
gmax = params.gmax; % G/cm, max grad amp
gslew = params.gslew; % G/cm/ms max slew
passmean = params.passmean;
passw = params.passw;
stopmax = params.stopmax;
tbf = params.tbf;
tbz = params.tbz;
slthickz = params.slthickz;
display = params.display;
dt = params.dt; % gradient/rf sampling rate
tipangle = params.tipangle; % tip angle of iterative spsp pulse
sloff = params.sloff; % cm, slice offset

gambar = 4258;       % gamma/2pi in Hz/g
gam = gambar*2*pi;   % gamma in radians/g

roughpenalty = 1; % switch to use roughness penalty, to avoid
% 'conolly wings'
roughbeta = 10; % roughness regularization param

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spectral parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

T = tbf/passw; % duration of pulse from mid of first lobe to mid of last lobe, seconds
fovf = 2*stopmax; % Hz, freq FOV we need
Ts = 1/fovf; % subpulse duration, seconds
nlobes = ceil(T/Ts+1); % number of RF subpulses
if params.nospsp == 1
    nlobes = 1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get gradient waveform
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
kz_w = tbz / slthickz; % cycles/cm - necessary gradient area
gz_area = kz_w / 4258; % G/cm * s
nsamp = ceil(Ts/dt); % 
if ~flyback
    nsamp = 2*nsamp;
end
verse_frac = 0;0.9;
[gpos, gneg, g1, g2, g3] = grad_ss(gz_area, nsamp, verse_frac, gmax, ...
    gslew, dt, ~flyback);

% stitch together the gradient. Build an RF mask as well.
g = [];
rfmask = [];
if flyback
    for ii = 1:nlobes
        if ii < nlobes
            g = [g gpos gneg];
            rfmask = [rfmask zeros(size(g1)) ones(size(g2)) zeros(size(g3)) ...
                zeros(size(gneg))];
        else
            g = [g gpos]; % don't want to add refocusing lobe at end, we'll
            % just build an optimal one for z.
            rfmask = [rfmask zeros(size(g1)) ones(size(g2)) ...
                zeros(size(g3))];
        end
    end
    % design final refocusing lobe to place kz=0 at the center of the
    % positive traps
    garea = gz_area/2; % G/cm*s
    garea = abs(sum(gpos))*dt/2; % G/cm*s
    gref = grad_mintrap(garea, gmax, gslew, dt);
    g = [g -gref];
    rfmask = [rfmask zeros(size(gref))];
    rfmask = logical(rfmask);
else
    for ii = 1:nlobes
        g = [g gpos];
        gpos = -1*gpos;
        rfmask = [rfmask zeros(size(g1)) ones(size(g2)) zeros(size(g3))];
        %rfmask = [rfmask zeros(1,nramp) ones(1,length(gpos)-2*nramp) zeros(1,nramp)];
    end
    % design final refocusing lobe to place kz=0 at the center of the
    % last trap
    garea = abs(sum(gpos))*dt/2; % G/cm*s
    gref = grad_mintrap(garea, gmax, gslew, dt);
    g = [g sign(sum(gpos))*gref];
    rfmask = [rfmask zeros(size(gref))];
    rfmask = logical(rfmask);
end
greflen = length(gref);

% integrate gradient to get kz(t)
gupsampfact = 4;
gint = interp(g,gupsampfact);
kz = -gambar * dt/gupsampfact * fliplr(cumsum(fliplr(gint),2));
kz = kz(1:gupsampfact:end);
kzd = kz(rfmask);

% get kf
tt = (0:dt:length(g)*dt-dt);
kf = tt-(length(g)-1)*dt;
kfd = kf(rfmask);

% select desired effective TE period at end of pulse
% linear phase pulse will have TE starting 1/2 way through the
% pulse
TEeff = length(g)*dt*1000 - (nlobes-1)/2*Ts*1000 - length(gpos)/2*dt*1000; % ms

disp(['Effective echo time (TEeff): ' num2str(TEeff) ' ms']);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build desired pattern and error weights
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
osfact = 16; % design grid oversampling factor
fovz = 1/dt / gambar / mean(g2);
Nz = length(g2)*osfact;

d1 = 0.01;% pass- and stop-band ripple (in normalized
% flip angle units)
wz = dinf(d1,d1)/tbz; % Dinfinity function - transition width in z
edgesz = [1-wz 1+wz]*slthickz/2; % edges in z
wf = dinf(d1,d1)/tbf; %
edgesf = [max(0,1-wf) 1+wf]*passw/2;

% set up the design grid
Nf = ceil(fovf*length(g)*dt)*osfact; % number of spectral grid points
if rem(Nf,2) ~= 0
    Nf = Nf + 1;
end
[zz,ff] = ndgrid(-fovz/2:fovz/Nz:fovz/2-fovz/Nz,[-fovf/2:fovf/Nf:fovf/2-fovf/Nf]);

% build error weighting matrix
dpass = double(abs(zz) <= edgesz(1) & abs(ff) <= edgesf(1));
dpass = circshift(dpass,[0,round(passmean/(fovf/Nf))]);
dpass = circshift(dpass,[round(sloff/(fovz/Nz)),0]);
dstop = double(abs(zz) >= edgesz(2) | abs(ff) >= edgesf(2));
dstop = circshift(dstop,[0,round(passmean/(fovf/Nf))]);
dstop = circshift(dstop,[round(sloff/(fovz/Nz)),0]);
maskw = dpass + dstop;
W = diag_sp(col(maskw));

% apply phase roll to get final target pattern
d = dpass.*exp(-1i*TEeff*2*pi/1000*ff);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build system matrix and roughness penalty and perform design
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
J = 6;Kz = 2*Nz;Kf = 2*Nf; % nufft params
nufft_args = {[Nz Nf],[J J],[Kz Kf],[Nz Nf]/2,'minmax:kb'}; % nufft params
G = Gmri_SENSE([kzd.' kfd.'],logical(ones([Nz Nf])),'fov',[fovz fovf], ...
    'basis',{'dirac'},'nufft',nufft_args,'exact',0)';

if roughpenalty
    % get roughness penalty
    R = (roughbeta)*spdiags(single(ones(sum(rfmask),1)),1,sum(rfmask),sum(rfmask));
else
    R = 0;
end

iters = ceil(sum(rfmask)/20); % # of CG iterations
iters = 20;
disp(['Number of CG iterations: ' num2str(iters)]);
% run CG to design pulse
[xS,info] = qpwls_pcg(zeros(length(kzd),1),G,W,d,0, ...
    R,1,iters,ones(size(d)));

rf = zeros(size(rfmask));
rf(rfmask) = xS(:,end)*tipangle*pi/180/(1i*gam*dt);

% get final mag pattern
m = G * xS(:,end);m = reshape(m,Nz,Nf);
disp(['Design NRMSE: ' num2str(nrmse(d(:),m(:),maskw(:)))]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulate pulse and display results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if display
    simus = 2; % simulation upsamp factor
    % simulate at z = sloff across spectral dim
    fsim = -fovf/2:fovf/Nf/simus:fovf/2-fovf/Nf/simus;
    [foo,mbl_f,mblz_f] = blochsim_spinor_slsel(Nf*simus,fovf,dt,rf.*exp(1i*2*pi*kz*sloff),...
        2*pi/gam*ones(length(rf),1));
    % simulate at f = passmean across z dim
    zsim = -fovz/2:fovz/Nz/simus:fovz/2-fovz/Nz/simus;
    [foo,mbl_z,mblz_z] = blochsim_spinor_slsel(Nz*simus,fovz,dt,rf.*exp(1i*2*pi*passmean*[0:length(rf)-1]*dt),g);

    figure(777); hold on;
    % note that the magnitude should be high in water band, and zero in fat
    % band, but doesn't matter elsewhere
    plot(ff(1,:),sin(abs(d(Nz/2+1+round(sloff/(fovz/Nz)),:))*tipangle*pi/180),'k');
    hold on, grid on
    plot(fsim,abs(mbl_f),'r');
    title 'Spectral magnitude profile, z = sloff'
    xlabel 'f (Hz)'
    ylabel '|M_{xy}|'
    %legend('Desired','Simulated');
    drawnow;
    
    figure(888); hold on;
    % note that in this plot the simulated profile will look much
    % broader than the desired, since the transition regions are not depicted
    subplot(2,1,1);
    title 'Slice profile, f = passmean'
    plot(zz(:,Nf/2+1),sin(abs(d(:,Nf/2+1+round(passmean/(fovf/Nf))))*tipangle*pi/180),'k');
    grid on,hold on
    plot(zsim,abs(mbl_z),'r');
    xlabel 'z(cm)'
    subplot(2,1,2);
    grid on,hold on
    plot(zsim,unwrap(angle(mbl_z)),'r');
    drawnow;
    
    figure;
    subplot(1,2,1)
    imagesc(ff(1,:),zz(:,1),abs(d));title('target','fontsize',20);
    subplot(1,2,2);
    imagesc(ff(1,:),zz(:,1),abs(m));title('excited','fontsize',20);
end

% if bipolar then negate the phase of alternate lobes
if ~flyback && nlobes > 1
    % find begin and end index of each subpulse
    ind = find(diff(rfmask));
    ind = reshape(ind,[2,nlobes]);
    
    for nn = 1:nlobes
        rflobes(:,nn) = rf(ind(1,nn)+1:ind(2,nn));
        if mod(nn,2) == 0
            rflobes(:,nn)  = abs(rflobes(:,nn) ).*exp(-1i*angle(rflobes(:,nn)));
        end
    end
    rfbp = zeros(size(rfmask));
    rfbp(rfmask) = rflobes(:);
    rf = rfbp(:).';
end


