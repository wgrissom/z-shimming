function [g] = grad_mintrap (m0, mxg, mxs, ts)
%
% [g] = grad_mintrap (m0, mxg, mxs, ts)
%
% m0 	- target zeroth moment (G/cm * s)
% mxg	- maximum amplitude (G/cm)
% mxs   - maximum slew rate (G/cm/ms)
% ts    - sample time in s    
%
% g     - Gradient
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Adam B. Kerr
%
% (c)2007 Board of Trustees
% Leland Stanford Junior University
%
% $Header: /home/adam/cvsroot/src/ss/grad_mintrap.m,v 1.2 2007-08-31 00:00:50 adam Exp $
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% $Log: grad_mintrap.m,v $
% Revision 1.2  2007-08-31 00:00:50  adam
% Adding B1-minimization loop
%
% Revision 1.1.1.1  2007-08-29 15:58:20  adam
% Starting sources
%
% Revision 1.1.1.1  2007-08-29 15:45:31  adam
% Starting sources
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
if (m0 < 0)
	s = -1;
	m0 = -m0;
else
	s = 1;
end;

% Convert mxs to G/cm/s
%
mxs = mxs * 1e3;

% Determine trapezoid parameters
% 
% na - number of constant samples
% nb - number ramp samples
% A - trapezoid amplitude
%

dg = mxs * ts;				% Max delta in one sample
nb = ceil(sqrt (m0 / dg / ts));
A = m0/(nb*ts);
if (A <= mxg),
	na = 0;
	dg_act = A/nb;
else
	nb = ceil (mxg / dg);
	dg_act = mxg / nb;
	na = ceil((m0 - (nb^2 * dg_act * ts))/mxg/ts);
	dg_act = m0 / (nb^2 + na*nb)/ts;
	A = nb * dg_act;
end;

% Construct discrete trapezoid --- always end with a zero value
%
g = s * [[1:nb]*dg_act ones(1,na)*A [nb-1:-1:0]*dg_act];

return;


