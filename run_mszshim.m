
% Script to design multispectrum spectral-spatial pulses and z-shim
% gradients for through-plane signal loss compensation

clear

% design switches
Nbands = 2;
display = 0;
tipangle = 80;
nospsp = 0;

% pulse and gradient parameters
TE = 35e-3; % sec.
slthickz = 0.5; % cm, desired slice thickness
b0slthickz = 1; % cm
gmax = 3; % G/cm, max grad amp
gslew = 12; % G/cm/ms max slew
dt = 6.4e-6; % sec, gradient/rf dwell time
sloff = 0; % cm, slice offset from isocenter

load b0allslices
Nsl = size(b0,4);

for slind = 1:Nsl
    b0sl = -b0(:,:,:,slind);
    bandb0sl = -bandb0(:,:,:,slind);
    
    % common pulse design parameters
    params{1}.flyback = 1;
    params{1}.gmax = gmax;
    params{1}.gslew = gslew;
    params{1}.tbf = 3;
    params{1}.tbz = 4;
    params{1}.fixdur = 0;
    params{1}.slthickz = slthickz;
    params{1}.sloff = 0;
    params{1}.dt = dt;
    params{1}.display = display;
    params{1}.tipangle = tipangle;
    params{1}.nospsp = nospsp; % switch to design a single pulse for comparison to standard z-shim
    
    for ii = 1:Nbands
        if ii > 1
            params{ii} = params{1}; % inherit common parameters
        end
        tmp = bandb0sl(:,:,ii);
        y = tmp(bandmask(:,:,ii,slind));
        x = linspace(min(y),max(y),64);
        h = hist(y,x);
        [~,imax] = max(h);
        params{ii}.passmean = x(imax);
        params{ii}.passw = 160;
        params{ii}.stopmax = 220;
    end
    
    % If we are designing a simple slice-select pulse then just design one
    % pulse that excites the entire slice
    if nospsp
        for ii = 1:Nbands
            params{ii}.passmean = 0;
            params{ii}.passw = 220;
            params{ii}.stopmax = 100;
        end
    end
    
    % Design the SPSP pulses
    for ii = 1:Nbands
        [rf{ii},gz{ii},dz{ii},mz{ii},TEeff(ii),greflen(ii)] = dz_mszshim(params{ii});
        TEeff(ii) = TEeff(ii)/1000; % s
    end
    
    [Nx,Ny,Nsl] = size(b0sl);
    
    %% 3D simulation
    for ii = 1:Nbands
        Ns = length(rf{ii});
        tvec = (-Ns/2:Ns/2-1)*dt;
        kx = zeros(Ns,1);
        ky = zeros(Ns,1);
        kz = -4258*flipud(cumsum(flipud(col(gz{ii}))))*6.4e-6;
        k = [kx,ky,kz];
        J = 6;
        L = 12;
        fov = [26 26 b0slthickz];
        
        maskall = repmat(mask(:,:,slind),[1 1 Nsl]);
        nufft_args = {[Nx Ny Nsl],[J J J],[2*Nx 2*Ny 2*Nsl],[Nx/2 Ny/2 Nsl/2],'minmax:kb'};
        A3d = Gmri(k,maskall,'fov',fov,'basis',{'dirac'},...
            'nufft',nufft_args,'exact',0,...
            'ti',tvec,'L',L,'zmap',1i*2*pi*b0sl.*maskall)';
        
        % simulate at z = 0
        rf_z = rf{ii}.*exp(1i*2*pi*kz.'*sloff);
        mall(:,:,:,ii) = embed(A3d*rf_z.',maskall);
    end
    
    %% Find the optimal z-shim gradient moment for each band
    TErem = repmat(TE,[1 Nbands]);
    zz = linspace(-b0slthickz/2,b0slthickz/2,Nsl);
    zz = repmat(zz,[Nx 1 Ny]);
    zz = permute(zz,[1 3 2]);
    
    nshims = 32;
    alphas = linspace(-2e-4,2e-4,nshims); % G/cm/Hz
    bandmaskall = repmat(bandmask(:,:,:,slind),[1 1 1 nshims]);
    bandmaskall = permute(bandmaskall,[1 2 4 3]);
    
    zshim_str = [];
    bestalpha = [];
    m_bestshim = [];
    for ii = 1:Nbands
        zshims = alphas'*params{ii}.passmean; % g/cm
        zshimsr = repmat(zshims,[1 Nx Ny Nsl]);
        zshimsr = permute(zshimsr,[2 3 4 1]);
        clear m_shimmedr
        for jj = 1:nshims
            % apply b0 phase
            mb0 = (mall(:,:,:,ii)).*exp(1i*2*pi*b0sl*TErem(ii));
            % apply z-shim gradient
            mb0re = mb0.*...
                exp(1i*2*pi*4258*zshimsr(:,:,:,jj).*zz*TErem(ii));
            sig = sum(mb0re,3);
            m_zshimmed(:,:,jj,ii) = sig;
            m_shimmedr(:,jj) =  sig(bandmask(:,:,ii,slind));
        end
        [~,maxind] = max(sum(abs(m_shimmedr),1));
        bestalpha = [bestalpha; alphas(maxind)];
        bestshim = zshims(maxind);
        m_bestshim = cat(3,m_bestshim,m_zshimmed(:,:,maxind,ii));
        
        % calculate the z-shim strength to set in the protocol
        zshimarea = bestshim*TErem(ii);
        dur = 1e-3; % s, duration of z-shim waveform
        slope = 0.2e-3; % s duration of ramps
        zshim_str = [zshim_str; zshimarea/(dur-slope)*10]; % mT/m
    end
    figure; subplot(2,1,1); im(m_bestshim); title 'excited bands'
    subplot(2,1,2); im(sqrt(sum(abs(m_bestshim.^2),3))); title 'sum-of-squares combined'
    drawnow;
end


