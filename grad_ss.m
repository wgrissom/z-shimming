function [gpos,gneg,g1,g2,g3] = grad_ss(m0, n, f, mxg, mxs, ts, equal)
% GRAD_SS - Calculate spectral-spatial bipolar pulse 
%   
% [gpos, gneg, g1, g2, g3] = grad_ss (m0, n, f, mxg, mxs, ts, equal)
%
% m0 	- target zeroth moment (G/cm * s) of one lobe
% n     - total number of samples to use if not []
% f     - fraction of ramp to include in bridge [0..1]    
% mxg	- maximum amplitude (G/cm)
% mxs   - maximum slew rate (G/cm/ms)
% ts    - sample time in s    
% equal - boolean if pos/neg lobes should be same
%
% gpos  - Positive lobe gradient
% gneg  - Negative lobe gradient
% g1, g2, g3 - Ramp up, bridge, ramp down gradients
%              in positive lobe
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Adam B. Kerr
%
% (c)2007 Board of Trustees
% Leland Stanford Junior University
%
% NOT TO BE USED OUTSIDE MRSRL WITHOUT WRITTEN PERMISSION OF AUTHOR
%
% $Header: /home/adam/cvsroot/src/ss/grad_ss.m,v 1.7 2007-10-24 17:37:19 adam Exp $
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% $Log: grad_ss.m,v $
% Revision 1.7  2007-10-24 17:37:19  adam
% Add some tolerance on max grad, max slew checks
%
% Revision 1.6  2007-10-05 16:41:41  adam
% Maintain convention that last sample of ramp is always included, regardless of
% 'f' value
%
% Revision 1.5  2007-09-19 03:10:30  adam
% Fix bug that non-equal lobes are not returned when "n" is unspecified.
%
% Revision 1.4  2007-09-09 18:51:38  adam
% Add check for number of ramp samples going to 1.
%
% Revision 1.3  2007-09-07 17:26:47  adam
% Integrating ss_spect_correct, as well as EP support, almost(!!) working
%
% Revision 1.2  2007-09-05 20:59:24  adam
% Add water/fat examples
% First pass at adding symmetric filter design---still needs some work
% Get rid of SLR trial in EPI spectral correct routine---will fix later
%
% Revision 1.1  2007-09-05 17:53:21  adam
% Add support for SS gradient design with verse fraction
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

m0 = abs(m0);				% Must be positive

% Check n is even if equal lobes called for
%
if equal, 
    if bitget(n,1) ~= 0, 
	error('equal lobes specified, but n not even');
    end;
end;

% Convert mxs to G/cm/s
%
mxs_s = mxs * 1e3;
dg = mxs_s * ts;				% Max delta in one sample

% Determine trapezoid parameters
% 
% na - number of constant samples
% nb - number ramp samples
% nc - number samples of ramp in bridge
% A - trapezoid amplitude
%

% Do different things if number of samples is specified
%
[gp_tmp, g1_tmp, g2_tmp, g3_tmp] = grad_min_bridge(m0, f, mxg, mxs, ts);
if equal, 
    gn_tmp = -gp_tmp;
else
    m0_pos = sum(gp_tmp) * ts;
    gn_tmp = grad_mintrap(-m0_pos, mxg, mxs, ts);
end;

if isempty(n), 
    gpos = gp_tmp;
    g1 = g1_tmp;
    g2 = g2_tmp;
    g3 = g3_tmp;
    gneg = gn_tmp;
else
    if (length([gp_tmp gn_tmp]) > n)
	error('grad_ss: Solution not obtained in spec num samples');
    end;

    % Save known solution
    %
    gp_save = gp_tmp;
    g1_save = g1_tmp;
    g2_save = g2_tmp;
    g3_save = g3_tmp;
    gn_save = -gp_save;
    
    nb_save = find(diff(gp_save) == 0, 1, 'first');
    
    % Now keep decreasing number of ramp samples in 
    % positive lobe until "n" exceeded
    %
    spec_met = 1;
    while (spec_met && (nb_save > 1))
	% Get area in ramps
	%
	nb = nb_save - 1;
	nc = max(1,ceil(nb*f));
	a_ramps = (2*nb-nc+1)*nc * dg * ts;
	
	% Get number of constant samples
	%
	a_const = m0 - a_ramps;
	na = max(0,ceil(a_const/(nb*dg*ts)));
	
	% Get correct amplitude, gradients now
	%
	dg_test = m0 / ( ((2*nb-nc+1)*nc + nb*na) *ts);
	A = nb * dg_test;
	if ((A > mxg) || (dg_test > dg)), 
	    spec_met = 0;
	    continue;
	end;
	g1 = [1:(nb-nc)] * dg_test;
	g2 = [[(nb-nc+1):nb] nb*ones(1,na) [nb:-1:(nb-nc+1)]] * dg_test;
	g3 = [(nb-nc):-1:0] * dg_test;
	gp = [g1 g2 g3];
	if abs((sum(g2)*ts) - m0) > 10*eps, 
	    error('grad_ss: Area not calculated correctly');
	end;
	
	if (equal)
	    gn = -gp;
	else
	    gn = grad_mintrap(-sum(gp)*ts, mxg, mxs, ts);
	end;
	
	% See if spec still met
	%
	if length([gp gn]) < n, 
	    spec_met = 1;
	    
	    gp_save = gp;
	    g1_save = g1;
	    g2_save = g2;
	    g3_save = g3;
	    gn_save = gn;
	    nb_save = nb;
	else
	    spec_met = 0;
	end;
    end;
    
    % Fix up result to have "exactly" n samples in it!
    %
    if ~equal
	na = n - length(gn_save) - (2 * nb_save + 1);
    else
	na = (n - 2*(2 * nb_save + 1))/2;
    end;
    nb = nb_save;
    nc = max(1,ceil(nb*f));

    % Get correct amplitude, gradients now
    %
    dg_test = m0 / ( ((2*nb-nc+1)*nc + nb*na) *ts);
    A = nb * dg_test;
    if ((A >= 1.001 * mxg) || (dg_test > 1.001 * dg)), 
	error('Amp/Slew being exceeded');
    end;
    g1 = [1:(nb-nc)] * dg_test;
    g2 = [[(nb-nc+1):nb] nb*ones(1,na) [nb:-1:(nb-nc+1)]] * dg_test;
    g3 = [(nb-nc):-1:0] * dg_test;
    gpos = [g1 g2 g3];
    if abs((sum(g2)*ts) - m0) > 10*eps, 
	error('grad_ss: Area not calculated correctly');
    end;

    if (~equal)
	ratio = sum(-gn_save)/sum(gpos);
	if (ratio < 1-10*eps)  
	    %	warning('grad_ss: Improbable ratio');   % fix problem here
	end;
	gneg = gn_save / ratio;
    else
	gneg = -gpos;
    end;
end;


return;


