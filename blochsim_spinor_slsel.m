function [state,mbl,mzbl] = blochsim_spinor_slsel(zdim,FOVz,dt,b1,g)

% 1-d bloch sim using spinor formulation
% assumes b1 is same length as g
% assumes initial magnetization is along z

gambar = 4258;               % gamma/2pi in Hz/g
gam = gambar*2*pi;         % gamma in radians/g

if zdim > 1
  z = -FOVz/2:FOVz/zdim:FOVz/2-FOVz/zdim;
else
  z = 0;
end

state = [ones(zdim,1);zeros(zdim,1)];

% apply pulse
btot = ones(zdim,1)*b1(:).';
gtot = z(:)*g(:).';

phi = dt*gam*(abs(btot).^2+gtot.^2).^0.5;
nxy = dt*gam*(phi.^(-1)).*btot;nxy(find(isnan(nxy))) = 0;
nz = dt*gam*(phi.^(-1)).*gtot;nz(find(isnan(nz))) = 0;

cp = cos(phi/2);sp = sin(phi/2);
clear phi;

for tt = 1:length(b1)

  alpha = cp(:,tt)+1i*nz(:,tt).*sp(:,tt);
  beta = 1i*conj(nxy(:,tt)).*sp(:,tt);
    
  tmp = zeros(2*zdim,1);
  tmp(1:zdim) = alpha.*state(1:zdim) + beta.* ...
      state(zdim+1:end);
  tmp(zdim+1:end) = -conj(beta).*state(1:zdim) + ...
      conj(alpha).*state(zdim+1:end);
  
  state = tmp;
  
end

alpha_end = state(1:zdim,end);
minconjbeta_end = state(zdim+1:end,end);

% calculate final magnetization state
mbl = 2*conj(alpha_end).*minconjbeta_end;
mzbl = alpha_end.*conj(alpha_end) - minconjbeta_end.*conj(minconjbeta_end);
