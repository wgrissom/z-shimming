function [g, g1, g2, g3] = grad_min_bridge(m0, f, mxg, mxs, ts)
% GRAD_MIN_BRIDGE - Determine gradient trapezoid that gives required area from middle "bridge" section
%
% [g, g1, g2, g3] = grad_min_bridge (m0, f, mxg, mxs, ts)
%
% m0 	- target zeroth moment (G/cm * s)
% f  - fraction of ramp to include in bridge [0..1]    
% mxg	- maximum amplitude (G/cm)
% mxs   - maximum slew rate (G/cm/ms)
% ts    - sample time in s    
%
% g     - Gradient
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Adam B. Kerr
%
% (c)2007 Board of Trustees
% Leland Stanford Junior University
%
% NOT TO BE USED OUTSIDE MRSRL WITHOUT WRITTEN PERMISSION OF AUTHOR
%
% $Header: /home/adam/cvsroot/src/ss/grad_min_bridge.m,v 1.3 2007-08-30 05:21:18 adam Exp $
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% $Log: grad_min_bridge.m,v $
% Revision 1.3  2007-08-30 05:21:18  adam
% Fixed up some nuisance bugs.
% Saw some strange behaviour with 'verse' and so modified it slightly
% into ss_verse.  Still not sure it is best it can be.
%
% Revision 1.2  2007-08-30 00:23:57  adam
% Adding VERSE_FRACTION capability
%
% Revision 1.1  2007-08-29 23:48:42  adam
% Updated scripts to replicate Chuck's pulse---not sure C13
% bands are correct though...  Also added script to generate
% gradient lobes that start at non-zero values.
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
if (m0 < 0)
	s = -1;
	m0 = -m0;
else
	s = 1;
end;

% Convert mxs to G/cm/s
%
mxs = mxs * 1e3;

% Determine trapezoid parameters
% 
% na - number of constant samples
% nb - number ramp samples
% nc - number samples of ramp in bridge
% A - trapezoid amplitude
%

dg = mxs * ts;				% Max delta in one sample

% Assume triangle at first and see if max amp requirements met
% -- quadratic with aq, bq, cq coeffs
%
if (f ~= 0)
    aq = (2-f)*f;
    bq = f;
    cq = -m0/(dg*ts);
    nb = (-bq + sqrt(bq^2 - 4*aq*cq))/(2*aq);

    nb = ceil(nb);
    nc = max(1,ceil(nb*f));
else
    A = m0 / (2*ts);
    nb = ceil(A / dg);
    nc = 1;
end;

% Test result
%
dg_test = m0 / ((2*nb-nc+1)*nc*ts);
A = nb * dg_test;
if (A <= mxg) && (dg_test < dg),				% This works!
    g1 = s*[1:(nb-nc)] * dg_test;
    g2 = s*[[(nb-nc+1):nb] [nb:-1:(nb-nc+1)]] * dg_test;
    g3 = s*[(nb-nc):-1:0] * dg_test;
    g = [g1 g2 g3];
    if abs((sum(g2)*ts) - s*m0) > 10*eps, 
	fprintf(1,'Area Spec: %f Actual: %f\n', m0, sum(g)*ts);
	error('grad_min_bridge: Area not calculated correctly');
    end;
else  %% Must be trapezoid
    % Subtract area of ramps
    %
    nb = ceil(mxg/dg);
    nc = max(1,ceil(nb*f));
    dg_test = mxg/nb;
    a_ramps = (2*nb-nc+1)*nc * dg_test * ts;
    
    % get number of const samples
    %
    a_const = m0 - a_ramps;
    na = ceil(a_const/ts/mxg);
    
    % Get correct amplitude now
    %
    dg_test = m0 / ( ((2*nb-nc+1)*nc + nb*na) *ts);
    A = nb * dg_test;
    if ((A > mxg) || (dg_test > dg)), 
	error('Amp/Slew being exceeded');
    end;
    g1 = s* [1:(nb-nc)] * dg_test;
    g2 = s* [[(nb-nc+1):nb] nb*ones(1,na) [nb:-1:(nb-nc+1)]] * dg_test;
    g3 = s* [(nb-nc):-1:0] * dg_test;
    g = [g1 g2 g3];
    if abs((sum(g2)*ts) - s*m0) > 10*eps, 
	error('grad_min_bridge: Area not calculated correctly');
    end;
    
end;

return;


