
function out = nrmse(refimg,errimg,mask)

out = sqrt(sum(col((abs(refimg)-abs(errimg)).*mask).^2)/numel(mask));
